package engant.example.webintentbasic

import android.support.test.InstrumentationRegistry
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.robotium.solo.Solo
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumentation test, which will execute on an Android device.

 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
@RunWith(AndroidJUnit4::class)
class WebIntentRobotiumTest {
    /********************************************************************************/
    /***************** START - Instrumentation Tests ********************************/
    @get:Rule
    val activityActivityTestRule: ActivityTestRule<MainActivity> =
            ActivityTestRule(MainActivity::class.java)

    private var solo: Solo? = null

    @Before
    @Throws(Exception::class)
    fun setUp() {
        //setup() is run before a test case is started.
        //This is where the solo object is created.
        solo = Solo(InstrumentationRegistry.getInstrumentation(),
                activityActivityTestRule.activity)
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        //tearDown() is run after a test case has finished.
        //finishOpenedActivities() will finish all the activities that have been opened during the test execution.
        solo?.finishOpenedActivities()
    }

    /***************** END **********************************************************/

    @Test
    @Throws(Exception::class)
    fun testImplicitWebIntent() {
        solo?.unlockScreen()
        solo?.assertCurrentActivity("Wrong Activity!!!", MainActivity::class.java)
        solo?.enterText(0, "http://www.w3schools.com/xsl/xpath_intro.asp")
        solo?.clickOnButton(0)
    }

}
